[Input Unit (Service Firm)] 

A service firm's input unit is responsible for bringing in ingredients or raw materials.^

~Product Name~
The type of products or semi-products that the unit is currently purchasing.^
~Product Stock~
This area shows the stock of the product in the unit. Each box represents one cargo of the product. Each cargo can store a certain number of units of the product. ^
~Supply and Demand Bar~
The yellow bar indicates the supply of the product by the supplier. The orange bar indicates the demand of the product from the current firm. If the supply bar is shorter than the demand bar, that means there is a lack of supply. Overall firm performance may be affected by the lack of the supply. In this case, you should consider switching to a better supplier. ^
~Utilization Bar~
This brown bar shows the utilization of the unit. If this bar reaches its full length, the unit has reached its full capacity.
