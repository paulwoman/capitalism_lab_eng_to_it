[TEXT]
NIL 

[ACTION]
Hide=Preview

//-------------------------------------------------------------------//

[TEXT]
In this tutorial you will set up a farm to supply your supermarkets with your own agricultural products. Such backward integration will create a more reliable supply chain and increase your profit margins. 

[POS]
Flex Text Box=300,280,400,0
Next Arrow=Yes


//-------------------------------------------------------------------//


[TEXT]
In this game, a farm can produce meat, milk, eggs, leather and a variety of crops.^^To build your first farm, click the ~Build~ icon.  

[POS]
Text Box=100,180,400,0
Pointer=3,25,52,62

//-------------------------------------------------------------------//

[TEXT]
Select ~Farm~ from the menu. 

[POS]
Text Box=100,200,300,0
Pointer=28,85,215,100

//-------------------------------------------------------------------//

[TEXT]
There are two sizes of farms available. Select ~Farm - Medium~ from the menu and choose a location in a rural area to build your farm. 

[POS]
Text Box=610,318,380,0
Pointer=240,40,416,95

[CONDITION]
Firm Build Count=Farm, 1 

[ACTION]
Set firm description=My Farm 

//-------------------------------------------------------------------//

[TEXT]
The first thing we are going to do is to raise some livestock.^^~Double click the top left slot~ and build a ~Livestock Raising Unit~. 

[POS]
Text Box=32,250,350,0
Pointer=489,253,573,336

[CONDITION]
Unit Exists=1, Livestock Raising

//-------------------------------------------------------------------//

[TEXT]
Now you need to set the type of animals to be raised. Click the ~Raise Livestock~ button and then select ~Cattle~ from the popup menu.

[POS]
Text Box=32,460,350,0
Pointer=256,386,374,400

[CONDITION]
Unit has item=1, Cattle

//-------------------------------------------------------------------//

[TEXT]
Good! Your farm has begun rearing cattle.^^To the right of the existing Livestock Raising Unit, build a ~Livestock Processing Unit~.

[POS]
Text Box=32,250,350,0
Pointer=599,253,680,336

[CONDITION]
Unit Exists=2, Livestock Processing

//-------------------------------------------------------------------//

[TEXT]
Now click the link between the units to connect them. 

[POS]
Text Box=32,250,350,0
Pointer=579,292,592,298

[CONDITION]
Unit Linked=Livestock Raising, Livestock Processing, 1, 2

//-------------------------------------------------------------------//

[TEXT]
NIL

[ACTION]
Fast forward days=5

//-------------------------------------------------------------------//

[TEXT]
Products of cattle include ~frozen beef~, ~milk~, and ~leather~. Each ~Livestock Processing Unit~ can only process one product at a time.^^Now click ~Select Product~ and select ~Frozen Beef~ from the available choices. 

[POS]
Text Box=150,120,600,0
Pointer=255,384,372,401

[CONDITION]
Unit has item=2, Frozen Beef

//-------------------------------------------------------------------//

[TEXT]
Your cattle will now be reared and processed as frozen beef.^^Add a ~Sales Unit~ and link it to the ~Livestock Processing Unit~ so that your farm can sell the frozen beef.

[POS]
Text Box=142,462,690,0
Pointer=709,253,790,336

[CONDITION]
Unit Exists=3, Sales

//-------------------------------------------------------------------//

[TEXT]
Now click the link between the units to connect them. 

[POS]
Text Box=32,250,350,0
Pointer=687,292,702,298

[CONDITION]
Unit Linked=Livestock Processing, Sales, 2, 3

//-------------------------------------------------------------------//

[TEXT]
NIL

[ACTION]
Fast forward days=5

//-------------------------------------------------------------------//

[TEXT]
Congratulations, you are now selling frozen beef reared on your own farm.^^You may set up additional ~Livestock Processing Units~ to process ~milk~ and ~leather~ from cattle.

[POS]
Text Box=300,280,400,0
Next Arrow=Yes

//-------------------------------------------------------------------//

[TEXT]
You may also grow crops in farms by using ~Crop Growing Units~. Growing crops is a time consuming process and it has to go through sowing, growing and harvesting before you will see the products coming out from the unit.

[POS]
Text Box=300,280,400,0
Next Arrow=Yes


//-------------------------------------------------------------------//


[TEXT]
The production of many consumer goods is dependent on agricultural products. You can use the Farmer's Guide to see which products can be made from a certain crop or livestock. 

[POS]
Text Box=300,280,400,0
Next Arrow=Yes

//-------------------------------------------------------------------//

[TEXT]
You can open the ~Farmer's Guide~ by pressing the shortcut key F2 or by selecting it from the Information Center.^^Let's open it now by pressing F2. 

[POS]
Text Box=300,280,400,0

[CONDITION]
Active Interface=Farmer Guide 

//-------------------------------------------------------------------//

[TEXT]
This guide covers all types of crops and livestock in the game. For the sake of this tutorial, let's select ~Cattle~.

[POS]
Text Box=300,280,400,0
Pointer=496,410,626,421 

//-------------------------------------------------------------------//

[TEXT]
The screen shows that Frozen Beef, Milk and Leather are products of cattle. To find out what products can be made from Milk, just click on the Milk icon.

[POS]
Text Box=300,450,400,0
Pointer=486,227,590,335 


//-------------------------------------------------------------------//

[TEXT]
It shows that Bottled Milk can be produced using from Milk. You can click the [MORE] button to see other products that can be made from Milk.

[POS]
Text Box=300,450,400,0
Pointer=349,184,395,195

//-------------------------------------------------------------------//

[TEXT]
Likewise, you may click on a crop's icon in the Farmer's Guide to learn more about the products made from that crop. 

[POS]
Text Box=300,450,400,0
Next Arrow=Yes

//-------------------------------------------------------------------//

[TEXT]
Now click the indicated icon at the bottom left of the screen to return to your farm. Note that you can click this icon whenever you want to return to your previous firm.

[POS]
Text Box=300,450,400,0
Pointer=193,0,233,30
Window=Bottom Toolbar 

//-------------------------------------------------------------------//

[TEXT]
In addition to manually setting up production units on your farm as you have learned, there is a tool available that can help you quickly set up a team of units for producing and selling agricultural goods.^^Click on the indicated icon to activate the tool now. 

[POS]
Text Box=300,266,450,0
Pointer=226,120,405,235

//-------------------------------------------------------------------//

[TEXT]
To have the game automatically set up the units for you, select a type of crop or livestock, and then click [SET UP ON FARM]. 

[POS]
Text Box=300,280,400,0
Pointer=525,177,686,191
Next Arrow=Yes

//-------------------------------------------------------------------//

[TEXT]
With the knowledge you have just gained, you can now set up a vertically integrated business that encompasses both farming and manufacturing. 

[POS]
Text Box=300,280,400,0
Next Arrow=Yes

//-------------------------------------------------------------------//

[TEXT]
To win this game, your company must make at least ~20 types of products~ to demonstrate its ability to produce a wide range of products.^^At the same time you must achieve ~an annual revenue of $100 million~ and ~an annual profit of $10 million~.

[POS]
Text Box=300,280,400,0
Next Arrow=Yes

