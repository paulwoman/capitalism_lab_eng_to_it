[Tech Programming Unit]

It is in charge of software technology development.^

~Technology~
The type of technology currently being developed.^
~R&D Progress~
The progress of the current R&D project.^
~Loyalty~
The employee loyalty of the team lead.