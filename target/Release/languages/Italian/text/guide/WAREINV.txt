[Warehouse Storage Unit]

It provides storage for a large quantity of raw materials or finished goods.^

~Product Name~
The type of product in this unit.^
~Product Stock~
The blue bars indicate the quantity of products in the current warehouse storage unit.
